<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Person>
 */
class PersonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'first_name' => $this->faker->firstName(),
            'surname' => $this->faker->lastName(),
            'age' =>  $this->faker->numberBetween(18, 60),
            'address' =>  $this->faker->address(),
            'email' => $this->faker->unique()->safeEmail(),
            'occupation' => $this->faker->jobTitle(),
        ];
    }
}
