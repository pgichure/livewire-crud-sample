<?php

namespace App\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Person;

class PersonsRegister extends Component
{
    
    use WithPagination;
    
    public $sortField = 'first_name'; // default sorting field
    public $sortAsc = true; // default sort direction
    public $search = '';
    
    
    public function render()
    {
        return view('livewire.persons-register',[
            'persons' => Person::search($this->search)
            ->orderBy($this->sortField, $this->sortAsc ? 'asc' : 'desc')
            ->simplePaginate(10),
        ]);
    }
    
    
    public function sortBy($field)
    {
        if ($this->sortField === $field) {
            $this->sortAsc = !$this->sortAsc;
        } else {
            $this->sortAsc = true;
        }
        
        $this->sortField = $field;
    }
}
