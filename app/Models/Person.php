<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    /**
     * The table name
     */
    protected $table = 'persons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'surname',
        'age',
        'address',
        'email',
        'occupation'
    ];

    public static function search($query)
    {
        return empty($query) ? static::query() : static::where(function ($q) use ($query) {
            $q->where('first_name', 'LIKE', '%' . $query . '%')
                ->orWhere('surname', 'LIKE', '%' . $query . '%')
                ->orWhere('email', 'LIKE', '%' . $query . '%')
                ->orWhere('address', 'LIKE ', '%' . $query . '%')
                ->orWhere('occupation', 'LIKE', '%' . $query . '%');
        });
    }
}
